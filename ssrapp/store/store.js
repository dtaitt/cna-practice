import { createStore } from 'redux';
import { combineReducers } from 'redux';

export function numReducer(num = 0, action) {
	switch (action.type) {
	case 'ADD':
		return action.payload + 1
	default:
		return num;
	}
}

const rootReducer = combineReducers({num: numReducer})
const store = createStore(rootReducer);

export default store;
