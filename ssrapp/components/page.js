import React from 'react';
import { Provider } from 'react-redux';
import store from './../store/store'
const withStore = (PageType) => {
    return (
        class PageWrapper extends React.Component {
            render() {
                return (
                    <Provider store={store}>
                        <PageType />
                    </Provider>
                )
            }
        }
    )
}
export default withStore;