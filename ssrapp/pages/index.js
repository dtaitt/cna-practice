import { connect } from 'react-redux';
import withStore from '../components/page'
import Link from 'next/link'
import {testReducer} from '../store/store'
import {Component} from 'react'
import './index.scss'
import Butter from 'buttercms';
import REACT_APP_BUTTER_KEY from './../env'

const butter = Butter(REACT_APP_BUTTER_KEY);

class Index extends Component {
  state = {
    isLoaded: false,
  }

  componentDidMount() {
    butter.page.retrieve('*', 'charlemagne').then((response) => {
  // console.log(this)
  console.log(this.state.isLoaded)
      if (this.props.isLoaded !== true) {
        console.log(response.data.data.fields)
      }
      this.setState({isLoaded:true}, () => {console.log(this.state.isLoaded)})
    })
    // butter.page.retrieve('*', 'rowling').then(function(response) {
    //   console.log(response.data.data.fields)
    // })
  }
  
  render() {
    return (
       <div>
            <div>
                <Link href='/about'><h1>Home Page</h1></Link>
            </div>
            <p>{this.props.num}</p>
            <style jsx>{`
            `}</style>
        </div>
    );
  }
}

function mapStateToProps(state) {
	return {
		num: state.num
	};
};

export default withStore(connect(mapStateToProps)(Index))